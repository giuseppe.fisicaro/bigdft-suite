variables:
  SDK_DOCKER_IMAGE: "bigdft/sdk:latest"

default:
  image: $SDK_DOCKER_IMAGE #ubuntu:latest

stages:
  - build
  - check
  - lint
  - deploy

Basic:
  image: ubuntu:latest
  stage: build
  script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y install autoconf autotools-dev automake git python3-minimal python3-setuptools gcc make pkg-config gfortran libopenmpi-dev libblas-dev liblapack-dev curl cmake g++ python3-pip cython3 debhelper libyaml-dev
    - export JHBUILD_RUN_AS_ROOT="please do it"
    - export PYTHON=python3
    - python3 ./Installer.py -y autogen
    - mkdir tmp
    - cd tmp/
    - cp ../rcfiles/jhbuildrc jhbuildrc #Installer takes care of this
    - echo 'prefix = "/opt/bigdft-suite"' >> jhbuildrc
    - python3 ../Installer.py -y -f jhbuildrc build
    - python3 ../bundler/jhbuild.py -f jhbuildrc dist --dist-only bigdft-suite
    - apt -y install debhelper fakeroot
    - tar -xf bigdft-suite.tar.gz
    - cp -rp ../debian bigdft-suite/
    - export VERSION=$(head -n1 bigdft-suite/debian/changelog | sed "s/.*(\([0-9.]*\)-[0-9]*).*/\1/")
    - cp bigdft-suite.tar.gz bigdft-suite_${VERSION}.orig.tar.gz
    - mv bigdft-suite/ bigdft-suite-${VERSION}/
    - cd bigdft-suite-${VERSION}/
    - dpkg-buildpackage
  artifacts:
    expire_in: 1 hour
    paths:
      - tmp/bigdft-suite.tar.gz
      - tmp/bigdft-suite_*
      # - futile
      # - tmp/futile
      # - atlab
      # - tmp/atlab
      # - psolver
      # - tmp/psolver
      # - bigdft
      # - tmp/bigdft

OpenCL:
  image: $SDK_DOCKER_IMAGE
  stage: build
  script:
    - ./Installer.py -y autogen
    - mkdir tmp
    - cd tmp/
    - export PYTHON=python3
    - ../Installer.py -y -f ../rcfiles/ubuntu_OCL.rc build spred
  artifacts:
    expire_in: 1 hour
    paths:
      - futile
      - tmp/futile
      - atlab
      - tmp/atlab
      - liborbs
      - tmp/liborbs
      - psolver
      - tmp/psolver
      - bigdft
      - tmp/bigdft
      - tmp/install

.test_environment_variables: &test_environment_variables
  - export OMPI_MCA_btl_vader_single_copy_mechanism=none
  - export OMP_NUM_THREADS=2
  - export run_parallel="mpirun -np 3 --allow-run-as-root --oversubscribe"
# LD_LIBRARY_PATH is for libbabel.so
  - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/bigdft-suite/lib
  - export PYTHON=python3

.install_git: &install_git
    - apt-get update -y && apt-get install -yqqf openssh-client git unzip sshpass rsync --fix-missing
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git config --global user.email "bigdft.project@gmail.com"
    - git config --global user.name "BigDFT Gitlab Runner"
    - git clone https://github.com/ingydotnet/git-subrepo git-subrepo-repo
    - source git-subrepo-repo/.rc

.push_into_gitlab: &push_into_gitlab
    # Pull repo
    - echo "Pulling external repo into build"
    - if git remote -v | grep -Fq ${LIB_DIRECTORY}; then git remote set-url ${LIB_DIRECTORY} "git@gitlab.com:l_sim/${LIB_DIRECTORY}.git"; else git remote add ${LIB_DIRECTORY} "git@gitlab.com:l_sim/${LIB_DIRECTORY}.git"; fi
    - if git branch -v | grep -Fq ${LIB_DIRECTORY}/tmp_prepush; then git stash && git checkout ${LIB_DIRECTORY}/tmp_prepush && git stash pop; else git checkout -b ${LIB_DIRECTORY}/tmp_prepush ; fi
    - git status
    - git commit --allow-empty -am "Commit in upstream from bigdft-suite"
    - git remote -v
    - if git ls-remote --heads ${LIB_DIRECTORY} | grep -Fq ${CI_PROJECT_NAMESPACE}/upstream; then git push ${LIB_DIRECTORY} --delete ${CI_PROJECT_NAMESPACE}/upstream; else echo "Branch upstream doesn't exist"; fi 
    - git subrepo push ${LIB_DIRECTORY} -b ${CI_PROJECT_NAMESPACE}/upstream -r ${LIB_DIRECTORY}

.check_library:
  stage: check
  needs: [OpenCL]
  before_script:
    - *test_environment_variables
    - *install_git
    - *push_into_gitlab
    - source tmp/install/bin/bigdftvars.sh
  script:
    - cd tmp/${LIB_DIRECTORY}
    - make check
  artifacts:
    when: always
    paths:
      - tmp/${LIB_DIRECTORY}/tests

.check_directory:
  stage: check
  needs: [OpenCL]
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
    - cd tmp/bigdft/tests/${TEST_DIRECTORY}
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/${TEST_DIRECTORY}

futile:
  extends: .check_library
  variables:
    LIB_DIRECTORY: futile

atlab:
  extends: .check_library
  variables:
    LIB_DIRECTORY: atlab

liborbs:
  extends: .check_library
  variables:
    LIB_DIRECTORY: liborbs

psolver:
  extends: .check_library
  variables:
    LIB_DIRECTORY: psolver

bigdft-cubic:
  extends: .check_directory
  variables:
    TEST_DIRECTORY: DFT/cubic
  script:
    - CHECK_MODE=short make check
  retry: 1

bigdft-overDFT:
  extends: .check_directory
  variables:
    TEST_DIRECTORY: overDFT
  script:
    - CHECK_MODE=short make check
  retry: 1

bigdft-linear-minimal:
  extends: .check_directory
  variables:
    TEST_DIRECTORY: DFT/linear
  script:
    - CHECK_MODE=custom make check checkonly_that="base periodic surface"
  retry: 1

bigdft-linear-multipoles:
  stage: check
  needs: [OpenCL]
  before_script:
    - python3 -m pip install scipy
    - *test_environment_variables
    - cd tmp
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc build biopython
    - cd ../
    - source tmp/install/bin/bigdftvars.sh # employs the PyBigDFT of the client
  script:
    - cd tmp/bigdft/tests/DFT/linear
    - CHECK_MODE=custom make check checkonly_that="multipoles H2Omultipoles"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear
  retry: 1

ntpoly-check:
  stage: check
  needs: [OpenCL]
  script:
    - cd tmp/bigdft/tests/DFT/linear
# LD_LIBRARY_PATH is for libbabel.so
    - OMP_NUM_THREADS=2 run_parallel="mpirun -np 3" LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CI_PROJECT_DIR}/tmp/install/lib CHECK_MODE=custom make check checkonly_that="bigpoly"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear

bigdft-linear-extended:
  extends: .check_directory
  variables:
    TEST_DIRECTORY: DFT/linear
  script:
    - CHECK_MODE=custom make check checkonly_that="directmin cdft rotatesmall verysparse"
  retry: 1

pybigdft-examples:
  #image: ubuntu:latest
  stage: check
  needs: [OpenCL]
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
    - export BIGDFT_MPIRUN="mpirun -np 3"
  script:
    - wget -qO- http://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
    - eval "$(./bin/micromamba shell hook -s posix)"
    - micromamba create --name bigdft_client
    - micromamba activate bigdft_client
    - micromamba install -c conda-forge pip
    - pip install scipy future py3Dmol
    - micromamba install -c conda-forge dftd4
    - micromamba install -c conda-forge dftd4-python
    - micromamba install -c conda-forge simple-dftd3
    - micromamba install -c conda-forge dftd3-python    
    - micromamba install -c conda-forge matplotlib
    - mkdir pybigdft-examples
    - cd pybigdft-examples
    - export SECOND_PYTHONPATH=`echo $PYTHONPATH | sed s/:/\ /g | awk '{print $2}'`
    - python3 -m unittest discover -s $SECOND_PYTHONPATH/BigDFT/ -p "check_examples.py" | tee out.txt
  artifacts:
    when: always
    paths:
      - pybigdft-examples
      - tmp/PyBigDFT/Tests
  retry: 1

pybigdft-nbtests:
  stage: check
  before_script:
    - *test_environment_variables
    - source tmp_client/install/bin/bigdftvars.sh
    - export BigDFT_loglvl="debug"
    - export BigDFT_logpath=${CI_PROJECT_DIR}/tmp_tests
  script:
    - mkdir tmp_tests
    - cd tmp_tests
    - echo $BigDFT_loglvl
    - echo $BigDFT_logpath
    - pytest --nbval-lax --disable-warnings --ignore=_ignore ${CI_PROJECT_DIR}/PyBigDFT/Tests/
  artifacts:
    when: always
    paths:
      - tmp_tests*
      - tmp_tests/*
      - ${CI_PROJECT_DIR}/PyBigDFT/Tests
  needs: [client-build]

Sphinx:
  image: $SDK_DOCKER_IMAGE
  stage: build
  script:
    - export PYTHON=python3
    - python3 bundler/jhbuild.py --conditions=+devdoc -f rcfiles/jhbuildrc  build bigdft-client
    - pip install guzzle-sphinx-theme sphinxcontrib-programoutput nbsphinx
    - sphinx-apidoc -fe -t ${CI_PROJECT_DIR}/PyBigDFT/source/templates -o ${CI_PROJECT_DIR}/PyBigDFT/source ${CI_PROJECT_DIR}/PyBigDFT/ *AiidaCalc* *setup.py
    - PYTHONPATH=$PWD/install/lib/python3.7/site-packages/ sphinx-build bigdft-doc build
    - mkdir ${CI_PROJECT_DIR}/coverage/
    - mv ${CI_PROJECT_DIR}/bigdft-doc/coverage.yaml ${CI_PROJECT_DIR}/coverage/
  artifacts:
    paths:
      - "PyBigDFT"
      - "futile"
      - "build"
      - "coverage"
  coverage: '/percentage.*?:(.*)/'

flake8:
  stage: lint
  needs: []
  script:
    - flake8 PyBigDFT/BigDFT/*.py --ignore=W291,W504,E121,E123,E126,E226,E24,E704
  allow_failure: true

pages:
  stage: deploy
  # only:
  #   - tags@l_sim/bigdft-suite
  before_script:
    - echo -n
  script:
    - mv build ${CI_PROJECT_DIR}/public/
    - mv PyBigDFT/build ${CI_PROJECT_DIR}/public/PyBigDFT
    - mv futile/build ${CI_PROJECT_DIR}/public/futile
    - mv tmp/bigdft-suite* ${CI_PROJECT_DIR}/public/
  artifacts:
    paths:
      - public
  needs: ["Basic", "OpenCL", "Sphinx"]  # deploy pages after build

dist-install:
  stage: deploy
  needs: [Basic]
  script:
    - tar -xf tmp/bigdft-suite.tar.gz
    - mkdir compile && cd compile
    - export PYTHON=python3
    - python3 ../bigdft-suite/bundler/jhbuild.py -f ../bigdft-suite/rcfiles/ubuntu_OCL.rc build
    

client-build:
  image: ubuntu:latest
  stage: build
  script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y install autoconf autotools-dev automake git python3-minimal python3-setuptools curl gcc python3-dev make python3-pip cython3
    - export JHBUILD_RUN_AS_ROOT="please do it"
    - export PYTHON=python3
    - python3 ./Installer.py -y autogen atlab
    - mkdir tmp_client
    - cd tmp_client/
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc build biopython
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc --conditions=+ase,+dill build bigdft-client
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc dist --dist-only bigdft-client
  artifacts:
    expire_in: 2 hour
    paths:
      - tmp_client/install
      - tmp_client/futile
      - tmp_client/PyBigDFT
