AC_PREREQ(2.59)

AC_INIT([liborbs - a library for orbital treatments in DFT],[0.1],[Damien.Caliste@cea.fr],[liborbs])

AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE()

AC_PROG_FC([], [Fortran 90])
AC_LANG(Fortran)
AC_FC_SRCEXT(f90)

AX_FC_OPENMP()
AX_FC_TYPE_MISMATCH()
AX_FC_MOD()
AX_FC_MODULEDIR()

AC_CHECK_TOOL(RANLIB, ranlib, [AC_MSG_ERROR(["No 'ranlib' program found."])])
AC_CHECK_TOOL(AR, ar, [AC_MSG_ERROR(["No 'ar' program found."])])

dnl Search for pkg-config
PKG_PROG_PKG_CONFIG()

AX_DYNAMIC_LIBRARIES("no", [daub_to_isf], [])
AX_MPI()
AX_ATLAB()

dnl Use the simple or the optimised convolution code.
AC_MSG_CHECKING([for convolution optimisation])
ac_use_opti_convolut="yes"
AC_ARG_ENABLE(optimised-convolution, AS_HELP_STRING([--disable-optimised-convolution], [Use the simple convolution code (use the optimised code by default).]),
                         ac_use_opti_convolut=$enableval, ac_use_opti_convolut="yes")
AM_CONDITIONAL(USE_OPTI_CONVOLUT, test "$ac_use_opti_convolut" = "yes")
AC_MSG_RESULT([$ac_use_opti_convolut])
dnl Test libraries required for benching / testing.
AC_LANG_PUSH(C++)
AC_CHECK_LIB(config++, config_init, withconfig=yes, withconfig=no)
AM_CONDITIONAL(HAVE_LIBCONFIG, test "$withconfig" = "yes")
AC_LANG_POP(C++)

dnl Accelerator support.
AX_ACC_CUDA()
AX_ACC_OCL()
AX_ACC_MIC()

dnl Agregate all the optional libs.
LIB_ORBS_DEPS="$LIBOCL_LIBS $LIB_ATLAB_LIBS $LIB_FUTILE_LIBS"
if test $ax_FUTILE_static != "yes" ; then
   LIB_ORBS_DEPS="$LIB_ORBS_DEPS $LIBCUDA_LIBS $LINALG_LIBS $MPI_LDFLAGS $MPI_LIBS"
fi
LIB_ORBS_PKG=""
if test $ax_build_dynamic != "yes" ; then
  LIB_ORBS_PKG="$LIB_ORBS_DEPS"
fi

AC_SUBST(LIB_ORBS_PKG)
AC_SUBST(LIB_ORBS_DEPS)

dnl Give the name of file.in to transform to file
AC_CONFIG_FILES([
Makefile
liborbs.pc
src/Makefile
src/convolutions-c/Makefile
tests/Makefile
tests/unit-tests/Makefile
])

AC_OUTPUT

echo "
Liborbs configure summary:
=========================

Basics:
  Fortran90 compiler:        $FC
  Fortran90 flags:           $FCFLAGS
  Linker flags:              $LDFLAGS
  Linked libraries:          $LIB_ORBS_DEPS $LIBS

Options:
  With OpenMP:               $ax_fc_openmp_msg
  With MPI:                  $ac_use_mpi
   | include dir.:           $MPI_INCLUDE
   | linker flags:           $MPI_LDFLAGS
   | linked libraries:       $MPI_LIBS
   | MPI_INIT_THREAD avail:  $ac_with_mpinithread
   | MPI2 support:           $ac_with_mpi2
  With optimised conv.:      $ac_use_opti_convolut
  With Cuda GPU conv.:       $ax_have_acc_cuda
   | NVidia Cuda Compiler:   $NVCC
   | Cuda flags:             $CUDA_FLAGS
  With OpenCL support:       $ax_have_acc_ocl
  With Intel MIC support:    $ax_have_acc_mic

Installation paths:
  Source code location:      ${srcdir}
  Prefix:                    $prefix
  Exec prefix:               $exec_prefix
  Binaries:                  $bindir
  Static libraries:          $libdir
"

if test x"$ac_use_mpi" = x"warn" ; then
  echo "Warnings:"
  if test x"$ac_use_mpi" = x"warn" ; then
    echo "  MPI:                       disabled by tests"
  fi
fi
