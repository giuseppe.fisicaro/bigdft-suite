program test
  use locregs
  implicit none

  type(locreg_descriptors) :: lr

  lr = locreg_null()

  call deallocate_locreg_descriptors(lr)

  call precong_per
end program test
