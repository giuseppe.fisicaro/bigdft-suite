subroutine bind_f90_f_progress_bar_copy_constructor( &
    out_self, &
    other)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), pointer :: out_self
  type(f_progress_bar), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_progress_bar_copy_constructor

subroutine bind_f90_f_progress_bar_type_new( &
    out_self, &
    message, &
    ncall, &
    nstep, &
    t0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), pointer :: out_self
  integer, intent(in) :: ncall
  integer, intent(in) :: nstep
  integer(kind = f_long), intent(in) :: t0
  character(len = 90), intent(in) :: message

  nullify(out_self)
  allocate(out_self)
  out_self%message = message
  out_self%ncall = ncall
  out_self%nstep = nstep
  out_self%t0 = t0
end subroutine bind_f90_f_progress_bar_type_new

subroutine bind_f90_f_progress_bar_free( &
    self)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_f_progress_bar_free

subroutine bind_f90_f_progress_bar_empty( &
    out_self)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), pointer :: out_self

  nullify(out_self)
  allocate(out_self)
end subroutine bind_f90_f_progress_bar_empty

subroutine bind_f90_f_none_object_copy_constructor( &
    out_self, &
    other)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_none_object), pointer :: out_self
  type(f_none_object), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_none_object_copy_constructor

subroutine bind_f90_f_none_object_type_new( &
    out_self, &
    none, &
    none_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_none_object), pointer :: out_self
  integer(kind = f_long), intent(in) :: none_len
  character(len = none_len), intent(in) :: none

  nullify(out_self)
  allocate(out_self)
  out_self%none = none
end subroutine bind_f90_f_none_object_type_new

subroutine bind_f90_f_none_object_free( &
    self)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_none_object), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_f_none_object_free

subroutine bind_f90_f_dump_buffer_copy_constructor( &
    out_self, &
    other)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_dump_buffer), pointer :: out_self
  type(f_dump_buffer), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_dump_buffer_copy_constructor

subroutine bind_f90_f_dump_buffer_type_new( &
    out_self, &
    buf, &
    buf_len, &
    ipos)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_dump_buffer), pointer :: out_self
  integer(kind = f_long), intent(in) :: buf_len
  integer, intent(in) :: ipos
  character, dimension(buf_len), intent(in) :: buf

  nullify(out_self)
  allocate(out_self)
  out_self%buf = buf
  out_self%ipos = ipos
end subroutine bind_f90_f_dump_buffer_type_new

subroutine bind_f90_f_dump_buffer_free( &
    self)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_dump_buffer), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_f_dump_buffer_free

subroutine bind_f90_f_dump_buffer_empty( &
    out_self)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_dump_buffer), pointer :: out_self

  nullify(out_self)
  allocate(out_self)
end subroutine bind_f90_f_dump_buffer_empty

subroutine bind_f_none( &
    out_f_none)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_none_object), pointer :: out_f_none

  nullify(out_f_none)
  allocate(out_f_none)
  out_f_none = f_none( &
)
end subroutine bind_f_none

subroutine bind_f_time( &
    out_f_time)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_f_time

  out_f_time = f_time( &
)
end subroutine bind_f_time

subroutine bind_f_progress_bar_new( &
    out_bar, &
    nstep)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), pointer :: out_bar
  integer, optional, intent(in) :: nstep

  nullify(out_bar)
  allocate(out_bar)
  out_bar = f_progress_bar_new( &
    nstep)
end subroutine bind_f_progress_bar_new

subroutine bind_f_tty( &
    out_f_tty, &
    unit)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  logical :: out_f_tty
  integer, intent(in) :: unit

  out_f_tty = f_tty( &
    unit)
end subroutine bind_f_tty

subroutine bind_f_get_free_unit( &
    out_unt2, &
    unit)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer :: out_unt2
  integer, optional, intent(in) :: unit

  out_unt2 = f_get_free_unit( &
    unit)
end subroutine bind_f_get_free_unit

subroutine bind_f_getpid( &
    out_f_getpid)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer :: out_f_getpid

  out_f_getpid = f_getpid( &
)
end subroutine bind_f_getpid

subroutine bind_f_utils_errors( &
    )
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none

  call f_utils_errors( &
)
end subroutine bind_f_utils_errors

subroutine bind_update_progress_bar( &
    bar, &
    istep)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings
  use f_precisions
  use f_utils
  implicit none
  type(f_progress_bar), intent(inout) :: bar
  integer, intent(in) :: istep

  call update_progress_bar( &
    bar, &
    istep)
end subroutine bind_update_progress_bar

subroutine bind_f_pause( &
    sec)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: sec

  call f_pause( &
    sec)
end subroutine bind_f_pause

subroutine bind_f_utils_recl( &
    unt, &
    recl_max, &
    recl)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: unt
  integer, intent(in) :: recl_max
  integer, intent(out) :: recl

  call f_utils_recl( &
    unt, &
    recl_max, &
    recl)
end subroutine bind_f_utils_recl

subroutine bind_f_file_exists( &
    file, &
    file_len, &
    exists)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: file_len
  logical, intent(out) :: exists
  character(len = file_len), intent(in) :: file

  call f_file_exists( &
    file, &
    exists)
end subroutine bind_f_file_exists

subroutine bind_f_close( &
    unit)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: unit

  call f_close( &
    unit)
end subroutine bind_f_close

subroutine bind_f_file_unit( &
    file, &
    file_len, &
    unit)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: file_len
  integer, intent(out) :: unit
  character(len = file_len), intent(in) :: file

  call f_file_unit( &
    file, &
    unit)
end subroutine bind_f_file_unit

subroutine bind_f_mkdir( &
    dir, &
    dir_len, &
    path, &
    path_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: dir_len
  integer(kind = f_long), intent(in) :: path_len
  character(len = dir_len), intent(in) :: dir
  character(len = path_len), intent(out) :: path

  call f_mkdir( &
    dir, &
    path)
end subroutine bind_f_mkdir

subroutine bind_f_delete_file( &
    file, &
    file_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: file_len
  character(len = file_len), intent(in) :: file

  call f_delete_file( &
    file)
end subroutine bind_f_delete_file

subroutine bind_f_move_file( &
    src, &
    src_len, &
    dest, &
    dest_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: src_len
  integer(kind = f_long), intent(in) :: dest_len
  character(len = src_len), intent(in) :: src
  character(len = dest_len), intent(in) :: dest

  call f_move_file( &
    src, &
    dest)
end subroutine bind_f_move_file

subroutine bind_f_system( &
    command, &
    command_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: command_len
  character(len = command_len), intent(in) :: command

  call f_system( &
    command)
end subroutine bind_f_system

subroutine bind_f_rewind( &
    unit)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: unit

  call f_rewind( &
    unit)
end subroutine bind_f_rewind

subroutine bind_f_open_file( &
    unit, &
    file, &
    file_len, &
    status, &
    status_len, &
    position, &
    position_len, &
    action, &
    action_len, &
    binary)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: f_strcpy
  use f_precisions
  use f_utils
  implicit none
  integer, intent(inout) :: unit
  integer(kind = f_long), intent(in) :: file_len
  integer(kind = f_long), intent(in) :: status_len
  integer(kind = f_long), intent(in) :: position_len
  integer(kind = f_long), intent(in) :: action_len
  logical, optional, intent(in) :: binary
  character(len = file_len), intent(in) :: file
  character(len = status_len), optional, intent(in) :: status
  character(len = position_len), optional, intent(in) :: position
  character(len = action_len), optional, intent(in) :: action

  call f_open_file( &
    unit, &
    file, &
    status, &
    position, &
    action, &
    binary)
end subroutine bind_f_open_file

subroutine bind_f_diff_i( &
    n, &
    a_add, &
    b_add, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_integer) :: a_add
  integer(kind = f_integer) :: b_add
  integer(kind = f_integer), intent(out) :: diff

  call f_diff( &
    n, &
    a_add, &
    b_add, &
    diff)
end subroutine bind_f_diff_i

subroutine bind_f_diff_i2i1( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_integer), intent(out) :: diff
  integer(kind = f_integer), dimension(b_dim_0), intent(in) :: b
  integer(kind = f_integer), dimension(a_dim_0, &
 a_dim_1), intent(in) :: a

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i2i1

subroutine bind_f_diff_i3i1( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    a_dim_2, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: a_dim_2
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_integer), intent(out) :: diff
  integer(kind = f_integer), dimension(b_dim_0), intent(in) :: b
  integer(kind = f_integer), dimension(a_dim_0, &
 a_dim_1, &
 a_dim_2), intent(in) :: a

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i3i1

subroutine bind_f_diff_i2( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = 4), intent(out) :: diff
  integer(kind = 4), dimension(a_dim_0, &
 a_dim_1), intent(in) :: a
  integer(kind = 4), dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i2

subroutine bind_f_diff_i1( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = 4), intent(out) :: diff
  integer(kind = 4), dimension(a_dim_0), intent(in) :: a
  integer(kind = 4), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i1

subroutine bind_f_diff_i1i2( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = 4), intent(out) :: diff
  integer(kind = 4), dimension(a_dim_0), intent(in) :: a
  integer(kind = 4), dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i1i2

subroutine bind_f_diff_li( &
    n, &
    a_add, &
    b_add, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = 8), intent(inout) :: a_add
  integer(kind = 8), intent(inout) :: b_add
  integer(kind = 8), intent(out) :: diff

  call f_diff( &
    n, &
    a_add, &
    b_add, &
    diff)
end subroutine bind_f_diff_li

subroutine bind_f_diff_li2li1( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = 8), intent(out) :: diff
  integer(kind = 8), dimension(b_dim_0), intent(in) :: b
  integer(kind = 8), dimension(a_dim_0, &
 a_dim_1), intent(in) :: a

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_li2li1

subroutine bind_f_diff_li2( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = 8), intent(out) :: diff
  integer(kind = 8), dimension(a_dim_0, &
 a_dim_1), intent(in) :: a
  integer(kind = 8), dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_li2

subroutine bind_f_diff_li1( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = 8), intent(out) :: diff
  integer(kind = 8), dimension(a_dim_0), intent(in) :: a
  integer(kind = 8), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_li1

subroutine bind_f_diff_li1li2( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = 8), intent(out) :: diff
  integer(kind = 8), dimension(a_dim_0), intent(in) :: a
  integer(kind = 8), dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_li1li2

subroutine bind_f_diff_r( &
    n, &
    a_add, &
    b_add, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  real, intent(inout) :: a_add
  real, intent(inout) :: b_add
  real, intent(out) :: diff

  call f_diff( &
    n, &
    a_add, &
    b_add, &
    diff)
end subroutine bind_f_diff_r

subroutine bind_f_diff_d( &
    n, &
    a_add, &
    b_add, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  double precision, intent(inout) :: a_add
  double precision, intent(inout) :: b_add
  double precision, intent(out) :: diff

  call f_diff( &
    n, &
    a_add, &
    b_add, &
    diff)
end subroutine bind_f_diff_d

subroutine bind_f_diff_d1( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    diff, &
    ind)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  double precision, intent(out) :: diff
  integer(kind = f_long), optional, intent(out) :: ind
  double precision, dimension(a_dim_0), intent(in) :: a
  double precision, dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff, &
    ind)
end subroutine bind_f_diff_d1

subroutine bind_f_diff_d2d3( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    b_dim_1, &
    b_dim_2, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = f_long), intent(in) :: b_dim_2
  double precision, intent(out) :: diff
  double precision, dimension(a_dim_0, &
 a_dim_1), intent(in) :: a
  double precision, dimension(b_dim_0, &
 b_dim_1, &
 b_dim_2), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d2d3

subroutine bind_f_diff_d2d1( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  double precision, intent(out) :: diff
  double precision, dimension(b_dim_0), intent(in) :: b
  double precision, dimension(a_dim_0, &
 a_dim_1), intent(in) :: a

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d2d1

subroutine bind_f_diff_d3d1( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    a_dim_2, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: a_dim_2
  integer(kind = f_long), intent(in) :: b_dim_0
  double precision, intent(out) :: diff
  double precision, dimension(b_dim_0), intent(in) :: b
  double precision, dimension(a_dim_0, &
 a_dim_1, &
 a_dim_2), intent(in) :: a

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d3d1

subroutine bind_f_diff_d0d1( &
    n, &
    a, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  double precision, intent(inout) :: a
  integer(kind = f_long), intent(in) :: b_dim_0
  double precision, intent(out) :: diff
  double precision, dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d0d1

subroutine bind_f_diff_d2( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  double precision, intent(out) :: diff
  double precision, dimension(a_dim_0, &
 a_dim_1), intent(in) :: a
  double precision, dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d2

subroutine bind_f_diff_d3( &
    n, &
    a, &
    a_dim_0, &
    a_dim_1, &
    a_dim_2, &
    b, &
    b_dim_0, &
    b_dim_1, &
    b_dim_2, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: a_dim_1
  integer(kind = f_long), intent(in) :: a_dim_2
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  integer(kind = f_long), intent(in) :: b_dim_2
  real(kind = f_double), intent(out) :: diff
  real(kind = f_double), dimension(a_dim_0, &
 a_dim_1, &
 a_dim_2), intent(in) :: a
  real(kind = f_double), dimension(b_dim_0, &
 b_dim_1, &
 b_dim_2), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d3

subroutine bind_f_diff_d1d2( &
    n, &
    a, &
    a_dim_0, &
    b, &
    b_dim_0, &
    b_dim_1, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_dim_0
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(in) :: b_dim_1
  double precision, intent(out) :: diff
  double precision, dimension(a_dim_0), intent(in) :: a
  double precision, dimension(b_dim_0, &
 b_dim_1), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_d1d2

subroutine bind_f_diff_c1i1( &
    n, &
    a, &
    a_len, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_integer), intent(out) :: diff
  character, dimension(a_len), intent(in) :: a
  integer(kind = f_integer), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_c1i1

subroutine bind_f_diff_c1li1( &
    n, &
    a, &
    a_len, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(out) :: diff
  character, dimension(a_len), intent(in) :: a
  integer(kind = f_long), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_c1li1

subroutine bind_f_diff_c0i1( &
    n, &
    a, &
    a_len, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_integer), intent(out) :: diff
  character(len = a_len), intent(in) :: a
  integer(kind = f_integer), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_c0i1

subroutine bind_f_diff_c0li1( &
    n, &
    a, &
    a_len, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(out) :: diff
  character(len = a_len), intent(in) :: a
  integer(kind = f_long), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_c0li1

subroutine bind_f_diff_li0li1( &
    n, &
    a, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_long), intent(inout) :: a
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_long), intent(out) :: diff
  integer(kind = f_long), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_li0li1

subroutine bind_f_diff_i0i1( &
    n, &
    a, &
    b, &
    b_dim_0, &
    diff)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: n
  integer(kind = f_integer), intent(inout) :: a
  integer(kind = f_long), intent(in) :: b_dim_0
  integer(kind = f_integer), intent(out) :: diff
  integer(kind = f_integer), dimension(b_dim_0), intent(in) :: b

  call f_diff( &
    n, &
    a, &
    b, &
    diff)
end subroutine bind_f_diff_i0i1

subroutine bind_f_sizeof_i1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_integer), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_i1

subroutine bind_f_sizeof_i2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_i2

subroutine bind_f_sizeof_i3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_i3

subroutine bind_f_sizeof_i4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_i4

subroutine bind_f_sizeof_i5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_i5

subroutine bind_f_sizeof_li1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_li1

subroutine bind_f_sizeof_li2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_li2

subroutine bind_f_sizeof_li3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_li3

subroutine bind_f_sizeof_li4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_li4

subroutine bind_f_sizeof_li5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_li5

subroutine bind_f_sizeof_d1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  real(kind = f_double), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d1

subroutine bind_f_sizeof_d2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d2

subroutine bind_f_sizeof_d3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d3

subroutine bind_f_sizeof_d4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d4

subroutine bind_f_sizeof_d5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d5

subroutine bind_f_sizeof_d6( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4, &
    datatype_dim_5)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), intent(in) :: datatype_dim_5
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4, &
 datatype_dim_5), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d6

subroutine bind_f_sizeof_d7( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4, &
    datatype_dim_5, &
    datatype_dim_6)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), intent(in) :: datatype_dim_5
  integer(kind = f_long), intent(in) :: datatype_dim_6
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4, &
 datatype_dim_5, &
 datatype_dim_6), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_d7

subroutine bind_f_sizeof_r1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  real(kind = f_simple), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_r1

subroutine bind_f_sizeof_r2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_r2

subroutine bind_f_sizeof_r3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_r3

subroutine bind_f_sizeof_r4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_r4

subroutine bind_f_sizeof_r5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_r5

subroutine bind_f_sizeof_c0( &
    out_s, &
    datatype, &
    datatype_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_len
  character(len = datatype_len), intent(in) :: datatype

  out_s = f_sizeof( &
    datatype)
end subroutine bind_f_sizeof_c0

subroutine bind_f_size_i0( &
    out_s, &
    datatype)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_integer), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i0

subroutine bind_f_size_i1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_integer), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i1

subroutine bind_f_size_i2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i2

subroutine bind_f_size_i3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i3

subroutine bind_f_size_i4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i4

subroutine bind_f_size_i5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_integer), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_i5

subroutine bind_f_size_li1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_li1

subroutine bind_f_size_li2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_li2

subroutine bind_f_size_li3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_li3

subroutine bind_f_size_li4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_li4

subroutine bind_f_size_li5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_li5

subroutine bind_f_size_d0( &
    out_s, &
    datatype)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  real(kind = f_double), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d0

subroutine bind_f_size_d1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  real(kind = f_double), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d1

subroutine bind_f_size_d2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d2

subroutine bind_f_size_d3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d3

subroutine bind_f_size_d4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d4

subroutine bind_f_size_d5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d5

subroutine bind_f_size_d6( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4, &
    datatype_dim_5)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), intent(in) :: datatype_dim_5
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4, &
 datatype_dim_5), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d6

subroutine bind_f_size_d7( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4, &
    datatype_dim_5, &
    datatype_dim_6)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  integer(kind = f_long), intent(in) :: datatype_dim_5
  integer(kind = f_long), intent(in) :: datatype_dim_6
  real(kind = f_double), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4, &
 datatype_dim_5, &
 datatype_dim_6), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_d7

subroutine bind_f_size_r1( &
    out_s, &
    datatype, &
    datatype_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  real(kind = f_simple), dimension(datatype_dim_0), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_r1

subroutine bind_f_size_r2( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_r2

subroutine bind_f_size_r3( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_r3

subroutine bind_f_size_r4( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_r4

subroutine bind_f_size_r5( &
    out_s, &
    datatype, &
    datatype_dim_0, &
    datatype_dim_1, &
    datatype_dim_2, &
    datatype_dim_3, &
    datatype_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_dim_0
  integer(kind = f_long), intent(in) :: datatype_dim_1
  integer(kind = f_long), intent(in) :: datatype_dim_2
  integer(kind = f_long), intent(in) :: datatype_dim_3
  integer(kind = f_long), intent(in) :: datatype_dim_4
  real(kind = f_simple), dimension(datatype_dim_0, &
 datatype_dim_1, &
 datatype_dim_2, &
 datatype_dim_3, &
 datatype_dim_4), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_r5

subroutine bind_f_size_c0( &
    out_s, &
    datatype, &
    datatype_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long) :: out_s
  integer(kind = f_long), intent(in) :: datatype_len
  character(len = datatype_len), intent(in) :: datatype

  out_s = f_size( &
    datatype)
end subroutine bind_f_size_c0

subroutine bind_zero_string( &
    str, &
    str_len)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: f_strcpy
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: str_len
  character(len = str_len), intent(out) :: str

  call f_zero( &
    str)
end subroutine bind_zero_string

subroutine bind_zero_li( &
    val)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(out) :: val

  call f_zero( &
    val)
end subroutine bind_zero_li

subroutine bind_zero_i( &
    val)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_integer), intent(out) :: val

  call f_zero( &
    val)
end subroutine bind_zero_i

subroutine bind_zero_r( &
    val)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  real, intent(out) :: val

  call f_zero( &
    val)
end subroutine bind_zero_r

subroutine bind_zero_d( &
    val)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  double precision, intent(out) :: val

  call f_zero( &
    val)
end subroutine bind_zero_d

subroutine bind_zero_l( &
    val)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  logical, intent(out) :: val

  call f_zero( &
    val)
end subroutine bind_zero_l

subroutine bind_put_to_zero_r1( &
    da, &
    da_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  real(kind = f_simple), dimension(da_dim_0), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_r1

subroutine bind_put_to_zero_double( &
    n, &
    da)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: n
  double precision, intent(out) :: da

  call f_zero( &
    n, &
    da)
end subroutine bind_put_to_zero_double

subroutine bind_put_to_zero_double_1( &
    da, &
    da_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  double precision, dimension(da_dim_0), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_1

subroutine bind_put_to_zero_double_2( &
    da, &
    da_dim_0, &
    da_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  double precision, dimension(da_dim_0, &
 da_dim_1), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_2

subroutine bind_put_to_zero_double_3( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  double precision, dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_3

subroutine bind_put_to_zero_double_4( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2, &
    da_dim_3)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_long), intent(in) :: da_dim_3
  double precision, dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2, &
 da_dim_3), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_4

subroutine bind_put_to_zero_double_5( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2, &
    da_dim_3, &
    da_dim_4)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_long), intent(in) :: da_dim_3
  integer(kind = f_long), intent(in) :: da_dim_4
  double precision, dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2, &
 da_dim_3, &
 da_dim_4), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_5

subroutine bind_put_to_zero_double_6( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2, &
    da_dim_3, &
    da_dim_4, &
    da_dim_5)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_long), intent(in) :: da_dim_3
  integer(kind = f_long), intent(in) :: da_dim_4
  integer(kind = f_long), intent(in) :: da_dim_5
  double precision, dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2, &
 da_dim_3, &
 da_dim_4, &
 da_dim_5), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_6

subroutine bind_put_to_zero_double_7( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2, &
    da_dim_3, &
    da_dim_4, &
    da_dim_5, &
    da_dim_6)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_long), intent(in) :: da_dim_3
  integer(kind = f_long), intent(in) :: da_dim_4
  integer(kind = f_long), intent(in) :: da_dim_5
  integer(kind = f_long), intent(in) :: da_dim_6
  double precision, dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2, &
 da_dim_3, &
 da_dim_4, &
 da_dim_5, &
 da_dim_6), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_double_7

subroutine bind_put_to_zero_integer( &
    n, &
    da)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: n
  integer(kind = f_integer) :: da

  call f_zero( &
    n, &
    da)
end subroutine bind_put_to_zero_integer

subroutine bind_put_to_zero_integer1( &
    da, &
    da_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_integer), dimension(da_dim_0), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_integer1

subroutine bind_put_to_zero_integer2( &
    da, &
    da_dim_0, &
    da_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_integer), dimension(da_dim_0, &
 da_dim_1), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_integer2

subroutine bind_put_to_zero_integer3( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_integer), dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_integer3

subroutine bind_put_to_zero_long( &
    n, &
    da)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(in) :: n
  integer(kind = f_long) :: da

  call f_zero( &
    n, &
    da)
end subroutine bind_put_to_zero_long

subroutine bind_put_to_zero_long1( &
    da, &
    da_dim_0)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), dimension(da_dim_0), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_long1

subroutine bind_put_to_zero_long2( &
    da, &
    da_dim_0, &
    da_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), dimension(da_dim_0, &
 da_dim_1), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_long2

subroutine bind_put_to_zero_long3( &
    da, &
    da_dim_0, &
    da_dim_1, &
    da_dim_2)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: da_dim_0
  integer(kind = f_long), intent(in) :: da_dim_1
  integer(kind = f_long), intent(in) :: da_dim_2
  integer(kind = f_long), dimension(da_dim_0, &
 da_dim_1, &
 da_dim_2), intent(out) :: da

  call f_zero( &
    da)
end subroutine bind_put_to_zero_long3

subroutine bind_f_inc_i0( &
    i, &
    inc)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(inout) :: i
  integer, optional, intent(in) :: inc

  call f_increment( &
    i, &
    inc)
end subroutine bind_f_inc_i0

subroutine bind_f_ht_long( &
    out_time, &
    ns, &
    short_bn)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: ns
  logical, optional, intent(in) :: short_bn
  character(len = 95), intent(out) :: out_time

  out_time = f_humantime( &
    ns, &
    short_bn)
end subroutine bind_f_ht_long

subroutine bind_f_humantime( &
    out_time, &
    ns, &
    short_bn)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings
  use f_precisions
  use f_utils
  implicit none
  real(kind = f_double), intent(in) :: ns
  logical, optional, intent(in) :: short_bn
  character(len = 95), intent(out) :: out_time

  out_time = f_humantime( &
    ns, &
    short_bn)
end subroutine bind_f_humantime

subroutine bind_f_assert( &
    condition, &
    id, &
    id_len, &
    err_id, &
    err_name, &
    err_name_len)
  use f_precisions
  use yaml_strings
  use dictionaries
  use module_f_malloc, only: f_malloc_routine_name
  use f_utils
  implicit none
  logical, intent(in) :: condition
  integer(kind = f_long), intent(in) :: id_len
  integer, optional, intent(in) :: err_id
  integer(kind = f_long), intent(in) :: err_name_len
  character(len = id_len), intent(in) :: id
  character(len = err_name_len), optional, intent(in) :: err_name

  call f_assert( &
    condition, &
    id, &
    err_id, &
    err_name)
end subroutine bind_f_assert

subroutine bind_f_assert_str( &
    condition, &
    id, &
    err_id, &
    err_name, &
    err_name_len)
  use f_precisions
  use yaml_strings
  use dictionaries
  use f_utils
  implicit none
  logical, intent(in) :: condition
  type(f_string), intent(in) :: id
  integer, optional, intent(in) :: err_id
  integer(kind = f_long), intent(in) :: err_name_len
  character(len = err_name_len), optional, intent(in) :: err_name

  call f_assert( &
    condition, &
    id, &
    err_id, &
    err_name)
end subroutine bind_f_assert_str

subroutine bind_f_assert_double( &
    condition, &
    id, &
    id_len, &
    err_id, &
    err_name, &
    err_name_len, &
    tol)
  use f_precisions
  use yaml_strings
  use dictionaries
  use module_f_malloc, only: f_malloc_namelen
  use f_utils
  implicit none
  real(kind = f_double), intent(in) :: condition
  integer(kind = f_long), intent(in) :: id_len
  integer, optional, intent(in) :: err_id
  integer(kind = f_long), intent(in) :: err_name_len
  real(kind = f_double), optional, intent(in) :: tol
  character(len = id_len), intent(in) :: id
  character(len = err_name_len), optional, intent(in) :: err_name

  call f_assert( &
    condition, &
    id, &
    err_id, &
    err_name, &
    tol)
end subroutine bind_f_assert_double

subroutine bind_f_savetxt_d2( &
    file, &
    file_len, &
    data, &
    data_dim_0, &
    data_dim_1)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer(kind = f_long), intent(in) :: file_len
  integer(kind = f_long), intent(in) :: data_dim_0
  integer(kind = f_long), intent(in) :: data_dim_1
  character(len = file_len), intent(in) :: file
  real(kind = f_double), dimension(data_dim_0, &
 data_dim_1), intent(in) :: data

  call f_savetxt( &
    file, &
    data)
end subroutine bind_f_savetxt_d2

subroutine bind_f_get_option_l( &
    out_val, &
    default_bn, &
    opt)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  logical :: out_val
  logical, intent(in) :: default_bn
  logical, optional, intent(in) :: opt

  out_val = f_get_option( &
    default_bn, &
    opt)
end subroutine bind_f_get_option_l

subroutine bind_f_null_i0( &
    val, &
    nl)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  integer, intent(out) :: val
  type(f_none_object), intent(in) :: nl

  val = nl
end subroutine bind_f_null_i0

subroutine bind_f_null_r0( &
    val, &
    nl)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  real(kind = f_simple), intent(out) :: val
  type(f_none_object), intent(in) :: nl

  val = nl
end subroutine bind_f_null_r0

subroutine bind_f_null_d0( &
    val, &
    nl)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  real(kind = f_double), intent(out) :: val
  type(f_none_object), intent(in) :: nl

  val = nl
end subroutine bind_f_null_d0

subroutine bind_f_null_l0( &
    val, &
    nl)
  use dictionaries, only: f_err_throw, &
    dictionary, &
    dict_next, &
    dict_value, &
    dict_len, &
    max_field_length, &
    f_err_define, &
    dict_iter
  use yaml_strings, only: yaml_toa
  use f_precisions
  use f_utils
  implicit none
  logical, intent(out) :: val
  type(f_none_object), intent(in) :: nl

  val = nl
end subroutine bind_f_null_l0

