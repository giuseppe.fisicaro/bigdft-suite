subroutine bind_f90_f_string_copy_constructor( &
    out_self, &
    other)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_self
  type(f_string), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_string_copy_constructor

subroutine bind_f90_f_string_type_new( &
    out_self, &
    msg, &
    msg_len)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_self
  integer(kind = f_long), intent(in) :: msg_len
  character(len = msg_len), intent(in) :: msg

  nullify(out_self)
  allocate(out_self)
  out_self%msg = msg
end subroutine bind_f90_f_string_type_new

subroutine bind_f90_f_string_free( &
    self)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_f_string_free

subroutine bind_f90_f_string_empty( &
    out_self)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_self

  nullify(out_self)
  allocate(out_self)
end subroutine bind_f90_f_string_empty

subroutine bind_yaml_bold( &
    out_bstr, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: str_len
  character(len = 95), intent(out) :: out_bstr
  character(len = str_len), intent(in) :: str

  out_bstr = yaml_bold( &
    str)
end subroutine bind_yaml_bold

subroutine bind_yaml_blink( &
    out_bstr, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: str_len
  character(len = 95), intent(out) :: out_bstr
  character(len = str_len), intent(in) :: str

  out_bstr = yaml_blink( &
    str)
end subroutine bind_yaml_blink

subroutine bind_yaml_date_and_time_toa( &
    out_yaml_date_and_time_toa, &
    values, &
    zone)
  use f_precisions
  use yaml_strings
  implicit none
  logical, optional, intent(in) :: zone
  character(len = 95), intent(out) :: out_yaml_date_and_time_toa
  integer, dimension(8), optional, intent(in) :: values

  out_yaml_date_and_time_toa = yaml_date_and_time_toa( &
    values, &
    zone)
end subroutine bind_yaml_date_and_time_toa

subroutine bind_yaml_date_toa( &
    out_yaml_date_toa, &
    values)
  use f_precisions
  use yaml_strings
  implicit none
  character(len = 95), intent(out) :: out_yaml_date_toa
  integer, dimension(8), optional, intent(in) :: values

  out_yaml_date_toa = yaml_date_toa( &
    values)
end subroutine bind_yaml_date_toa

subroutine bind_yaml_time_toa( &
    out_yaml_time_toa, &
    values)
  use f_precisions
  use yaml_strings
  implicit none
  character(len = 95), intent(out) :: out_yaml_time_toa
  integer, dimension(8), optional, intent(in) :: values

  out_yaml_time_toa = yaml_time_toa( &
    values)
end subroutine bind_yaml_time_toa

subroutine bind_is_atoi( &
    out_yes, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_yes
  integer(kind = f_long), intent(in) :: str_len
  character(len = str_len), intent(in) :: str

  out_yes = is_atoi( &
    str)
end subroutine bind_is_atoi

subroutine bind_is_atoli( &
    out_yes, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_yes
  integer(kind = f_long), intent(in) :: str_len
  character(len = str_len), intent(in) :: str

  out_yes = is_atoli( &
    str)
end subroutine bind_is_atoli

subroutine bind_is_atof( &
    out_yes, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_yes
  integer(kind = f_long), intent(in) :: str_len
  character(len = str_len), intent(in) :: str

  out_yes = is_atof( &
    str)
end subroutine bind_is_atof

subroutine bind_is_atol( &
    out_yes, &
    str, &
    str_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_yes
  integer(kind = f_long), intent(in) :: str_len
  character(len = str_len), intent(in) :: str

  out_yes = is_atol( &
    str)
end subroutine bind_is_atol

subroutine bind_buffer_string( &
    string_bn, &
    string_lgt, &
    buffer, &
    buffer_len, &
    string_pos, &
    back, &
    istat)
  use f_precisions
  use yaml_strings
  implicit none
  integer, intent(in) :: string_lgt
  integer(kind = f_long), intent(in) :: buffer_len
  integer, intent(inout) :: string_pos
  logical, optional, intent(in) :: back
  integer, optional, intent(out) :: istat
  character(len = string_lgt), intent(inout) :: string_bn
  character(len = buffer_len), intent(in) :: buffer

  call buffer_string( &
    string_bn, &
    string_lgt, &
    buffer, &
    string_pos, &
    back, &
    istat)
end subroutine bind_buffer_string

subroutine bind_align_message( &
    rigid, &
    maxlen, &
    tabval, &
    anchor, &
    anchor_len, &
    message)
  use f_precisions
  use yaml_strings
  implicit none
  logical, intent(in) :: rigid
  integer, intent(in) :: maxlen
  integer, intent(in) :: tabval
  integer(kind = f_long), intent(in) :: anchor_len
  character(len = anchor_len), intent(in) :: anchor
  character(len = maxlen), intent(inout) :: message

  call align_message( &
    rigid, &
    maxlen, &
    tabval, &
    anchor, &
    message)
end subroutine bind_align_message

subroutine bind_read_fraction_string( &
    string_bn, &
    string_bn_len, &
    var, &
    ierror)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: string_bn_len
  double precision, intent(out) :: var
  integer, intent(out) :: ierror
  character(len = string_bn_len), intent(in) :: string_bn

  call read_fraction_string( &
    string_bn, &
    var, &
    ierror)
end subroutine bind_read_fraction_string

subroutine bind_rstrip( &
    string_bn, &
    string_bn_len, &
    substring, &
    substring_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: string_bn_len
  integer(kind = f_long), intent(in) :: substring_len
  character(len = string_bn_len), intent(inout) :: string_bn
  character(len = substring_len), intent(in) :: substring

  call rstrip( &
    string_bn, &
    substring)
end subroutine bind_rstrip

subroutine bind_shiftstr( &
    str, &
    str_len, &
    n)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: str_len
  integer, intent(in) :: n
  character(len = str_len), intent(inout) :: str

  call shiftstr( &
    str, &
    n)
end subroutine bind_shiftstr

subroutine bind_convert_f_char_ptr( &
    src, &
    src_len, &
    dest, &
    dest_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: src_len
  integer(kind = f_long), intent(in) :: dest_len
  character, dimension(src_len), intent(in) :: src
  character(len = dest_len), intent(out) :: dest

  call convert_f_char_ptr( &
    src, &
    dest)
end subroutine bind_convert_f_char_ptr

subroutine bind_yaml_itoa( &
    out_str, &
    data, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_integer), intent(in) :: data
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_str
  character(len = fmt_len), optional, intent(in) :: fmt

  out_str = yaml_toa( &
    data, &
    fmt)
end subroutine bind_yaml_itoa

subroutine bind_yaml_litoa( &
    out_str, &
    data, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: data
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_str
  character(len = fmt_len), optional, intent(in) :: fmt

  out_str = yaml_toa( &
    data, &
    fmt)
end subroutine bind_yaml_litoa

subroutine bind_yaml_ftoa( &
    out_str, &
    data, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  real(kind = f_simple), intent(in) :: data
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_str
  character(len = fmt_len), optional, intent(in) :: fmt

  out_str = yaml_toa( &
    data, &
    fmt)
end subroutine bind_yaml_ftoa

subroutine bind_yaml_dtoa( &
    out_str, &
    data, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  real(kind = f_double), intent(in) :: data
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_str
  character(len = fmt_len), optional, intent(in) :: fmt

  out_str = yaml_toa( &
    data, &
    fmt)
end subroutine bind_yaml_dtoa

subroutine bind_yaml_ltoa( &
    out_yaml_ltoa, &
    l, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical, intent(in) :: l
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_yaml_ltoa
  character(len = fmt_len), optional, intent(in) :: fmt

  out_yaml_ltoa = yaml_toa( &
    l, &
    fmt)
end subroutine bind_yaml_ltoa

subroutine bind_yaml_dvtoa( &
    out_vec_toa, &
    vec, &
    vec_dim_0, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: vec_dim_0
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_vec_toa
  real(kind = 8), dimension(vec_dim_0), intent(in) :: vec
  character(len = fmt_len), optional, intent(in) :: fmt

  out_vec_toa = yaml_toa( &
    vec, &
    fmt)
end subroutine bind_yaml_dvtoa

subroutine bind_yaml_ivtoa( &
    out_vec_toa, &
    vec, &
    vec_dim_0, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: vec_dim_0
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_vec_toa
  integer(kind = 4), dimension(vec_dim_0), intent(in) :: vec
  character(len = fmt_len), optional, intent(in) :: fmt

  out_vec_toa = yaml_toa( &
    vec, &
    fmt)
end subroutine bind_yaml_ivtoa

subroutine bind_yaml_rvtoa( &
    out_vec_toa, &
    vec, &
    vec_dim_0, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: vec_dim_0
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_vec_toa
  real, dimension(vec_dim_0), intent(in) :: vec
  character(len = fmt_len), optional, intent(in) :: fmt

  out_vec_toa = yaml_toa( &
    vec, &
    fmt)
end subroutine bind_yaml_rvtoa

subroutine bind_yaml_livtoa( &
    out_vec_toa, &
    vec, &
    vec_dim_0, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: vec_dim_0
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_vec_toa
  integer(kind = 8), dimension(vec_dim_0), intent(in) :: vec
  character(len = fmt_len), optional, intent(in) :: fmt

  out_vec_toa = yaml_toa( &
    vec, &
    fmt)
end subroutine bind_yaml_livtoa

subroutine bind_f_strcpy( &
    dest, &
    dest_len, &
    src, &
    src_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: dest_len
  integer(kind = f_long), intent(in) :: src_len
  character(len = dest_len), intent(out) :: dest
  character(len = src_len), intent(in) :: src

  call f_strcpy( &
    dest, &
    src)
end subroutine bind_f_strcpy

subroutine bind_f_strcpy_str( &
    dest, &
    dest_len, &
    src)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: dest_len
  type(f_string), intent(in) :: src
  character(len = dest_len), intent(out) :: dest

  call f_strcpy( &
    dest, &
    src)
end subroutine bind_f_strcpy_str

subroutine bind_string_equivalence( &
    out_ok, &
    a, &
    a_len, &
    b, &
    b_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_ok
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_len
  character(len = a_len), intent(in) :: a
  character(len = b_len), intent(in) :: b

  out_ok = (a .eqv. b)
end subroutine bind_string_equivalence

subroutine bind_string_inequivalence( &
    out_notok, &
    a, &
    a_len, &
    b, &
    b_len)
  use f_precisions
  use yaml_strings
  implicit none
  logical :: out_notok
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: b_len
  character(len = a_len), intent(in) :: a
  character(len = b_len), intent(in) :: b

  out_notok = (a .neqv. b)
end subroutine bind_string_inequivalence

subroutine bind_string_and_integer( &
    out_c, &
    a, &
    a_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_integer), intent(in) :: num
  character(len = a_len), intent(in) :: a

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_string_and_integer

subroutine bind_integer_and_string( &
    out_c, &
    a, &
    num, &
    num_len)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_integer), intent(in) :: a
  integer(kind = f_long), intent(in) :: num_len
  character(len = num_len), intent(in) :: num

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_integer_and_string

subroutine bind_integer_and_msg( &
    out_c, &
    a, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_integer), intent(in) :: a
  type(f_string), intent(in) :: num

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_integer_and_msg

subroutine bind_string_and_long( &
    out_c, &
    a, &
    a_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: a_len
  integer(kind = f_long), intent(in) :: num
  character(len = a_len), intent(in) :: a

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_string_and_long

subroutine bind_string_and_double( &
    out_c, &
    a, &
    a_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: a_len
  real(kind = f_double), intent(in) :: num
  character(len = a_len), intent(in) :: a

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_string_and_double

subroutine bind_string_and_simple( &
    out_c, &
    a, &
    a_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: a_len
  real(kind = f_simple), intent(in) :: num
  character(len = a_len), intent(in) :: a

  nullify(out_c)
  allocate(out_c)
  out_c = (a // num)
end subroutine bind_string_and_simple

subroutine bind_attach_ci( &
    out_c, &
    s, &
    s_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: s_len
  integer(kind = f_integer), intent(in) :: num
  character(len = s_len), intent(in) :: s

  nullify(out_c)
  allocate(out_c)
  out_c = (s + num)
end subroutine bind_attach_ci

subroutine bind_attach_cli( &
    out_c, &
    s, &
    s_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: s_len
  integer(kind = f_long), intent(in) :: num
  character(len = s_len), intent(in) :: s

  nullify(out_c)
  allocate(out_c)
  out_c = (s + num)
end subroutine bind_attach_cli

subroutine bind_attach_lic( &
    out_c, &
    num, &
    s, &
    s_len)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: num
  integer(kind = f_long), intent(in) :: s_len
  character(len = s_len), intent(in) :: s

  nullify(out_c)
  allocate(out_c)
  out_c = (num + s)
end subroutine bind_attach_lic

subroutine bind_attach_cd( &
    out_c, &
    s, &
    s_len, &
    num)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), pointer :: out_c
  integer(kind = f_long), intent(in) :: s_len
  real(kind = f_double), intent(in) :: num
  character(len = s_len), intent(in) :: s

  nullify(out_c)
  allocate(out_c)
  out_c = (s + num)
end subroutine bind_attach_cd

subroutine bind_msg_to_string( &
    string_bn, &
    string_bn_len, &
    msg)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: string_bn_len
  type(f_string), intent(in) :: msg
  character(len = string_bn_len), intent(out) :: string_bn

  string_bn = msg
end subroutine bind_msg_to_string

subroutine bind_string_to_msg( &
    msg, &
    string_bn, &
    string_bn_len)
  use f_precisions
  use yaml_strings
  implicit none
  type(f_string), intent(out) :: msg
  integer(kind = f_long), intent(in) :: string_bn_len
  character(len = string_bn_len), intent(in) :: string_bn

  msg = string_bn
end subroutine bind_string_to_msg

subroutine bind_yaml_itoa_fmt( &
    out_c, &
    num, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_integer), intent(in) :: num
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_c
  character(len = fmt_len), intent(in) :: fmt

  out_c = (num ** fmt)
end subroutine bind_yaml_itoa_fmt

subroutine bind_yaml_litoa_fmt( &
    out_c, &
    num, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: num
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_c
  character(len = fmt_len), intent(in) :: fmt

  out_c = (num ** fmt)
end subroutine bind_yaml_litoa_fmt

subroutine bind_yaml_dtoa_fmt( &
    out_c, &
    num, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  real(kind = f_double), intent(in) :: num
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_c
  character(len = fmt_len), intent(in) :: fmt

  out_c = (num ** fmt)
end subroutine bind_yaml_dtoa_fmt

subroutine bind_yaml_ctoa_fmt( &
    out_c, &
    num, &
    num_len, &
    fmt, &
    fmt_len)
  use f_precisions
  use yaml_strings
  implicit none
  integer(kind = f_long), intent(in) :: num_len
  integer(kind = f_long), intent(in) :: fmt_len
  character(len = 95), intent(out) :: out_c
  character(len = num_len), intent(in) :: num
  character(len = fmt_len), intent(in) :: fmt

  out_c = (num ** fmt)
end subroutine bind_yaml_ctoa_fmt

