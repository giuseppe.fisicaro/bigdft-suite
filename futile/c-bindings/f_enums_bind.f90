subroutine bind_f90_f_enumerator_copy_constructor( &
    out_self, &
    other)
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_self
  type(f_enumerator), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_enumerator_copy_constructor

subroutine bind_f90_f_enumerator_type_new( &
    out_self, &
    family, &
    id, &
    name)
  use f_precisions
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_self
  type(f_enumerator), pointer, optional :: family
  integer, optional, intent(in) :: id
  character(len = 64), optional, intent(in) :: name

  nullify(out_self)
  allocate(out_self)
  if (present(family)) out_self%family => family
  if (present(id)) out_self%id = id
  if (present(name)) out_self%name = name
end subroutine bind_f90_f_enumerator_type_new

subroutine bind_f90_f_enumerator_free( &
    self)
  use f_enums
  implicit none
  type(f_enumerator), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_f_enumerator_free

subroutine bind_f_enumerator_null( &
    out_en)
  use yaml_strings, only: f_strcpy
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_en

  nullify(out_en)
  allocate(out_en)
  out_en = f_enumerator_null( &
)
end subroutine bind_f_enumerator_null

subroutine bind_nullify_f_enum( &
    en)
  use f_enums
  implicit none
  type(f_enumerator), intent(out) :: en

  call nullify_f_enum( &
    en)
end subroutine bind_nullify_f_enum

subroutine bind_f_enum_attr( &
    dest, &
    attr)
  use f_enums
  implicit none
  type(f_enumerator), intent(inout) :: dest
  type(f_enumerator), target, intent(inout) :: attr

  call f_enum_attr( &
    dest, &
    attr)
end subroutine bind_f_enum_attr

subroutine bind_f_enum_update( &
    dest, &
    src)
  use f_enums
  implicit none
  type(f_enumerator), intent(inout) :: dest
  type(f_enumerator), intent(in) :: src

  call f_enum_update( &
    dest, &
    src)
end subroutine bind_f_enum_update

subroutine bind_enum_is_enum( &
    out_ok, &
    en, &
    en1)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  type(f_enumerator), intent(in) :: en1

  out_ok = (en == en1)
end subroutine bind_enum_is_enum

subroutine bind_enum_is_int( &
    out_ok, &
    en, &
    int_bn)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer, intent(in) :: int_bn

  out_ok = (en == int_bn)
end subroutine bind_enum_is_int

subroutine bind_int_is_enum( &
    out_ok, &
    int_bn, &
    en)
  use f_enums
  implicit none
  logical :: out_ok
  integer, intent(in) :: int_bn
  type(f_enumerator), intent(in) :: en

  out_ok = (int_bn == en)
end subroutine bind_int_is_enum

subroutine bind_enum_is_char( &
    out_ok, &
    en, &
    char_bn, &
    char_bn_len)
  use yaml_strings, only: operator(.eqv.)
  use f_precisions
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer(kind = f_long), intent(in) :: char_bn_len
  character(len = char_bn_len), intent(in) :: char_bn

  out_ok = (en == char_bn)
end subroutine bind_enum_is_char

subroutine bind_enum_is_not_enum( &
    out_ok, &
    en, &
    en1)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  type(f_enumerator), intent(in) :: en1

  out_ok = (en /= en1)
end subroutine bind_enum_is_not_enum

subroutine bind_enum_is_not_int( &
    out_ok, &
    en, &
    int_bn)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer, intent(in) :: int_bn

  out_ok = (en /= int_bn)
end subroutine bind_enum_is_not_int

subroutine bind_enum_is_not_char( &
    out_ok, &
    en, &
    char_bn, &
    char_bn_len)
  use f_precisions
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer(kind = f_long), intent(in) :: char_bn_len
  character(len = char_bn_len), intent(in) :: char_bn

  out_ok = (en /= char_bn)
end subroutine bind_enum_is_not_char

subroutine bind_enum_has_attribute( &
    out_ok, &
    en, &
    family)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  type(f_enumerator), intent(in) :: family

  out_ok = (en .hasattr. family)
end subroutine bind_enum_has_attribute

subroutine bind_enum_has_char( &
    out_ok, &
    en, &
    family, &
    family_len)
  use f_precisions
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer(kind = f_long), intent(in) :: family_len
  character(len = family_len), intent(in) :: family

  out_ok = (en .hasattr. family)
end subroutine bind_enum_has_char

subroutine bind_enum_has_int( &
    out_ok, &
    en, &
    family)
  use f_enums
  implicit none
  logical :: out_ok
  type(f_enumerator), intent(in) :: en
  integer, intent(in) :: family

  out_ok = (en .hasattr. family)
end subroutine bind_enum_has_int

subroutine bind_enum_get_from_int( &
    out_iter, &
    en, &
    family)
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_iter
  type(f_enumerator), intent(in) :: en
  integer, intent(in) :: family

  nullify(out_iter)
  allocate(out_iter)
  out_iter = (en .getattr. family)
end subroutine bind_enum_get_from_int

subroutine bind_enum_get_from_char( &
    out_iter, &
    en, &
    family, &
    family_len)
  use f_precisions
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_iter
  type(f_enumerator), intent(in) :: en
  integer(kind = f_long), intent(in) :: family_len
  character(len = family_len), intent(in) :: family

  nullify(out_iter)
  allocate(out_iter)
  out_iter = (en .getattr. family)
end subroutine bind_enum_get_from_char

subroutine bind_enum_get_from_enum( &
    out_iter, &
    en, &
    family)
  use f_enums
  implicit none
  type(f_enumerator), pointer :: out_iter
  type(f_enumerator), intent(in) :: en
  type(f_enumerator), intent(in) :: family

  nullify(out_iter)
  allocate(out_iter)
  out_iter = (en .getattr. family)
end subroutine bind_enum_get_from_enum

subroutine bind_int_enum( &
    out_int_enum, &
    en)
  use f_enums
  implicit none
  integer :: out_int_enum
  type(f_enumerator), intent(in) :: en

  out_int_enum = toi( &
    en)
end subroutine bind_int_enum

