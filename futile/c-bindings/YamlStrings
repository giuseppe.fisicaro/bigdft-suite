#ifndef YAML_STRINGS_H
#define YAML_STRINGS_H

#include "futile_cst.h"

namespace Futile {

class FString
{
public:

  F_DEFINE_TYPE(f_string);
  operator f90_f_string*() { return F_TYPE(mFString); };
  operator const f90_f_string*() const { return F_TYPE(mFString); };
  operator f90_f_string_pointer*() { return &mFString; };
  FString(f90_f_string_pointer other) : mFString(other) {};

  FString(const FString& other);

  FString(const char* msg);

  ~FString(void);

  FString(void);

/*   void msg_and_msg(const FString& num) const; */

/*   void msg_and_string(const char* num) const; */

/*   void operator +(const FString& b) const; */

/*   void operator +(const char* b) const; */

  void assignment(const char* string_bn);

private:
  f90_f_string_pointer mFString;

};

void yaml_bold(char out_bstr[95],
    const char* str);

void yaml_blink(char out_bstr[95],
    const char* str);

void yaml_date_and_time_toa(char out_yaml_date_and_time_toa[95],
    const int (*values)[8] = nullptr,
    const bool (*zone) = nullptr);

void yaml_date_toa(char out_yaml_date_toa[95],
    const int (*values)[8] = nullptr);

void yaml_time_toa(char out_yaml_time_toa[95],
    const int (*values)[8] = nullptr);

bool is_atoi(const char* str);

bool is_atoli(const char* str);

bool is_atof(const char* str);

bool is_atol(const char* str);

/* void f_char_ptr(const char* str); */

void buffer_string(char* string_bn,
    int string_lgt,
    const char* buffer,
    int& string_pos,
    const bool (*back) = nullptr,
    int (*istat) = nullptr);

void align_message(bool rigid,
    int maxlen,
    int tabval,
    const char* anchor,
    char* message);

void read_fraction_string(const char* string_bn,
    double& var,
    int& ierror);

void rstrip(char* string_bn,
    size_t string_bn_len,
    const char* substring);

void shiftstr(char* str,
    size_t str_len,
    int n);

void convert_f_char_ptr(const char* src,
    char* dest,
    size_t dest_len);

void yaml_toa(char out_str[95],
    int data,
    const char (*fmt) = nullptr);

void yaml_toa(char out_str[95],
    size_t data,
    const char (*fmt) = nullptr);

void yaml_toa(char out_str[95],
    float data,
    const char (*fmt) = nullptr);

void yaml_toa(char out_str[95],
    double data,
    const char (*fmt) = nullptr);

/* void yaml_toa(const char* d,
    const char (*fmt) = nullptr); */

/* void yaml_toa(void); */

void yaml_toa(char out_yaml_ltoa[95],
    bool l,
    const char (*fmt) = nullptr);

void yaml_toa(char out_vec_toa[95],
    const double* vec,
    size_t vec_dim_0,
    const char (*fmt) = nullptr);

/* void yaml_toa(void); */

void yaml_toa(char out_vec_toa[95],
    const int* vec,
    size_t vec_dim_0,
    const char (*fmt) = nullptr);

/* void yaml_toa(void); */

/* void yaml_toa(void); */

void yaml_toa(char out_vec_toa[95],
    const float* vec,
    size_t vec_dim_0,
    const char (*fmt) = nullptr);

void yaml_toa(char out_vec_toa[95],
    const long* vec,
    size_t vec_dim_0,
    const char (*fmt) = nullptr);

void f_strcpy(char* dest,
    size_t dest_len,
    const char* src);

void f_strcpy(char* dest,
    size_t dest_len,
    const FString& src);

bool string_equivalence(const char* a,
    const char* b);

bool string_inequivalence(const char* a,
    const char* b);

FString string_and_integer(const char* a,
    int num);

FString integer_and_string(int a,
    const char* num);

FString integer_and_msg(int a,
    const FString& num);

FString string_and_long(const char* a,
    size_t num);

FString string_and_double(const char* a,
    double num);

FString string_and_simple(const char* a,
    float num);

/* void string_and_msg(const char* a,
    const FString& num); */

/* void combine_strings(const char* a,
    const char* b); */

/* void operator +(const char* a,
    const FString& b); */

FString attach_ci(const char* s,
    int num);

FString attach_cli(const char* s,
    size_t num);

FString attach_lic(size_t num,
    const char* s);

FString attach_cd(const char* s,
    double num);

void assignment(char* string_bn,
    size_t string_bn_len,
    const FString& msg);

void yaml_itoa_fmt(char out_c[95],
    int num,
    const char* fmt);

void yaml_litoa_fmt(char out_c[95],
    size_t num,
    const char* fmt);

void yaml_dtoa_fmt(char out_c[95],
    double num,
    const char* fmt);

void yaml_ctoa_fmt(char out_c[95],
    const char* num,
    const char* fmt);


}
#endif
