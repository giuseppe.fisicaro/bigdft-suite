#include "Misc"
#include "FEnums"
#include "YamlOutput"

using namespace Futile;

int main(int argc, char **argv)
{
  int en0_value = 0;
  const char en0_name[64] = "Primary";
  Futile::FEnumerator en0(0, &en0_value, &en0_name);
  int en1_value = 1;
  const char en1_name[64] = "Secondary";
  Futile::FEnumerator en1(0, &en1_value, &en1_name);

  Futile::initialize();
  
  Yaml::new_document();

  Yaml::sequence_open("Enumerators");
  Yaml::map("value", en0.toi());
  Yaml::map("value", en1.toi());
  Yaml::sequence_close();
  Yaml::map("Equality", en0 == en1);
  Yaml::map("Difference", en0 != en1);

  Futile::finalize();
  
  return 0;
}
