#include "iobox.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_read_field_dimensions, BIND_READ_FIELD_DIMENSIONS)(const char*,
  const size_t*,
  const char*,
  int*,
  int*,
  size_t,
  size_t);
void read_field_dimensions(const char* filename,
  char geocode,
  int ndims[3],
  int* nspin)
{
  size_t filename_chk_len, filename_len = filename_chk_len = filename ? strlen(filename) : 0;
  size_t geocode_chk_len = 1;
  FC_FUNC_(bind_read_field_dimensions, BIND_READ_FIELD_DIMENSIONS)
    (filename, &filename_len, &geocode, ndims, nspin, filename_chk_len, geocode_chk_len);
}

void FC_FUNC_(bind_read_field, BIND_READ_FIELD)(const char*,
  const size_t*,
  const char*,
  int*,
  double*,
  int*,
  const int*,
  const int*,
  double*,
  int*,
  f90_double_2d_pointer*,
  size_t*,
  size_t*,
  f90_int_1d_pointer*,
  size_t*,
  f90_int_1d_pointer*,
  size_t*,
  size_t,
  size_t);
void read_field(const char* filename,
  char geocode,
  int ndims[3],
  double hgrids[3],
  int* nspin,
  int ldrho,
  int nrho,
  double* rho,
  int (*nat),
  double_2d_pointer (*rxyz),
  int_1d_pointer (*iatypes),
  int_1d_pointer (*znucl))
{
  size_t filename_chk_len, filename_len = filename_chk_len = filename ? strlen(filename) : 0;
  size_t geocode_chk_len = 1;
  size_t rxyz_len[2];
  size_t iatypes_len[1];
  size_t znucl_len[1];
  FC_FUNC_(bind_read_field, BIND_READ_FIELD)
    (filename, &filename_len, &geocode, ndims, hgrids, nspin, &ldrho, &nrho, rho, nat, rxyz ? &rxyz->ptr : NULL, rxyz ? &rxyz_len[0] : NULL, rxyz ? &rxyz_len[1] : NULL, iatypes ? &iatypes->ptr : NULL, iatypes ? &iatypes_len[0] : NULL, znucl ? &znucl->ptr : NULL, znucl ? &znucl_len[0] : NULL, filename_chk_len, geocode_chk_len);
  if (rxyz) {; rxyz->len[0] = rxyz_len[0]; rxyz->len[1] = rxyz_len[1];};
  if (iatypes) {; iatypes->len = iatypes_len[0];};
  if (znucl) {; znucl->len = znucl_len[0];};
}

void FC_FUNC_(bind_dump_field, BIND_DUMP_FIELD)(const char*,
  const size_t*,
  const f90_cell*,
  const int*,
  const double*,
  const double*,
  const size_t*,
  const size_t*,
  const int*,
  const size_t*,
  const int*,
  const size_t*,
  const int*,
  const size_t*,
  const int*,
  size_t);
void dump_field(const char* filename,
  const f90_cell* mesh,
  int nspin,
  const double* rho,
  const double (*rxyz),
  size_t rxyz_dim_0,
  size_t rxyz_dim_1,
  const int (*iatype),
  size_t iatype_dim_0,
  const int (*nzatom),
  size_t nzatom_dim_0,
  const int (*nelpsp),
  size_t nelpsp_dim_0,
  const int (*ixyz0)[3])
{
  size_t filename_chk_len, filename_len = filename_chk_len = filename ? strlen(filename) : 0;
  FC_FUNC_(bind_dump_field, BIND_DUMP_FIELD)
    (filename, &filename_len, mesh, &nspin, rho, rxyz, &rxyz_dim_0, &rxyz_dim_1, iatype, &iatype_dim_0, nzatom, &nzatom_dim_0, nelpsp, &nelpsp_dim_0, ixyz0 ? *ixyz0 : NULL, filename_chk_len);
}

