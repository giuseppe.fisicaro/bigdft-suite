include(FortranCInterface)
FortranCInterface_HEADER(config.cmake MACRO_NAMESPACE "FC_")
add_custom_command(OUTPUT config.h
  COMMAND sed 's/FC_GLOBAL/FC_FUNC/' config.cmake > config.h)
FortranCInterface_VERIFY(CXX QUIET)

set(CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/include)

set(SOURCES f_functions.f90
    FiniteDiffCorff.inc
    harmonics.f90
    box.f90
    domain.f90
    IObox.f90
    ISF.f90
    mp_quadrature.f90
    numerics.f90
    openbabel_wrapper.f90
)

if(BABEL_FOUND)
  list(APPEND SOURCES openbabel.cpp config.h)
else()
  list(APPEND SOURCES openbabel_fake.c config.h)
endif()

if(ETSFIO_FOUND)
  list(APPEND SOURCES IOetsf.f90)
else()
  list(APPEND SOURCES IOetsf_fake.f90)
endif()

add_library(atlab ${SOURCES})
set_target_properties(atlab PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR})
target_include_directories(atlab PRIVATE ${FUTILE_INCLUDE_DIRS} ${BABEL_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(atlab PRIVATE ${FUTILE_LINK_LIBRARIES} ${BABEL_LINK_LIBRARIES} ${ETSFIO_LINK_LIBRARIES})
if(BUILD_OPENMP AND OpenMP_FOUND)
  target_compile_options(atlab PRIVATE ${OpenMP_Fortran_FLAGS})
endif()

install(TARGETS atlab LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
install(DIRECTORY ${CMAKE_Fortran_MODULE_DIRECTORY} DESTINATION ${CMAKE_INSTALL_PREFIX})
