subroutine bind_read_field_dimensions( &
    filename, &
    filename_len, &
    geocode, &
    ndims, &
    nspin)
  use box
  use f_precisions, only: f_long, &
    dp=>f_double, &
    gp=>f_double
  use dynamic_memory
  use f_enums
  use dictionaries, only: f_err_throw
  use at_domain
  use iobox
  implicit none
  integer(kind = f_long), intent(in) :: filename_len
  integer, intent(out) :: nspin
  character(len = filename_len), intent(in) :: filename
  character, intent(in) :: geocode
  integer, dimension(3), intent(out) :: ndims

  call read_field_dimensions( &
    filename, &
    geocode, &
    ndims, &
    nspin)
end subroutine bind_read_field_dimensions

subroutine bind_read_field( &
    filename, &
    filename_len, &
    geocode, &
    ndims, &
    hgrids, &
    nspin, &
    ldrho, &
    nrho, &
    rho, &
    nat, &
    rxyz, &
    rxyz_len_0, &
    rxyz_len_1, &
    iatypes, &
    iatypes_len_0, &
    znucl, &
    znucl_len_0)
  use box
  use f_precisions, only: f_long, &
    dp=>f_double, &
    gp=>f_double
  use dynamic_memory
  use f_enums
  use dictionaries, only: f_err_throw
  use at_domain
  use ioboxetsf, only: read_etsf
  use iobox
  implicit none
  integer(kind = f_long), intent(in) :: filename_len
  integer, intent(out) :: nspin
  integer, intent(in) :: ldrho
  integer, intent(in) :: nrho
  integer, optional, intent(out) :: nat
  integer(kind = f_long), optional, intent(out) :: rxyz_len_0
  integer(kind = f_long), optional, intent(out) :: rxyz_len_1
  integer(kind = f_long), optional, intent(out) :: iatypes_len_0
  integer(kind = f_long), optional, intent(out) :: znucl_len_0
  character(len = filename_len), intent(in) :: filename
  character, intent(in) :: geocode
  integer, dimension(3), intent(out) :: ndims
  real(kind = gp), dimension(3), intent(out) :: hgrids
  integer, dimension(:), optional, pointer :: iatypes
  integer, dimension(:), optional, pointer :: znucl
  real(kind = dp), dimension(ldrho, &
 nrho), intent(inout) :: rho
  real(kind = gp), dimension(:, :), optional, pointer :: rxyz

  call read_field( &
    filename, &
    geocode, &
    ndims, &
    hgrids, &
    nspin, &
    ldrho, &
    nrho, &
    rho, &
    nat, &
    rxyz, &
    iatypes, &
    znucl)
  if (present(rxyz)) then
    rxyz_len_0 = size(rxyz, 1)
    rxyz_len_1 = size(rxyz, 2)
  end if
  if (present(iatypes)) then
    iatypes_len_0 = size(iatypes, 1)
  end if
  if (present(znucl)) then
    znucl_len_0 = size(znucl, 1)
  end if
end subroutine bind_read_field

subroutine bind_dump_field( &
    filename, &
    filename_len, &
    mesh, &
    nspin, &
    rho, &
    rxyz, &
    rxyz_dim_0, &
    rxyz_dim_1, &
    iatype, &
    iatype_dim_0, &
    nzatom, &
    nzatom_dim_0, &
    nelpsp, &
    nelpsp_dim_0, &
    ixyz0)
  use box
  use f_precisions, only: f_long, &
    dp=>f_double, &
    gp=>f_double
  use f_harmonics
  use dynamic_memory
  use f_enums
  use dictionaries, only: f_err_throw
  use at_domain, only: domain_geocode
  use ioboxetsf, only: write_etsf_density
  use yaml_strings
  use f_utils
  use iobox
  implicit none
  integer(kind = f_long), intent(in) :: filename_len
  type(cell), intent(in) :: mesh
  integer, intent(in) :: nspin
  integer(kind = f_long), intent(in) :: rxyz_dim_0
  integer(kind = f_long), intent(in) :: rxyz_dim_1
  integer(kind = f_long), intent(in) :: iatype_dim_0
  integer(kind = f_long), intent(in) :: nzatom_dim_0
  integer(kind = f_long), intent(in) :: nelpsp_dim_0
  character(len = filename_len), intent(in) :: filename
  integer, dimension(iatype_dim_0), target, optional, intent(in) :: iatype
  integer, dimension(nzatom_dim_0), target, optional, intent(in) :: nzatom
  integer, dimension(nelpsp_dim_0), target, optional, intent(in) :: nelpsp
  integer, dimension(3), optional, intent(in) :: ixyz0
  real(kind = gp), dimension(rxyz_dim_0, &
 rxyz_dim_1), target, optional, intent(in) :: rxyz
  real(kind = dp), dimension(mesh%ndims(1), &
 mesh%ndims(2), &
 mesh%ndims(3), &
 nspin), intent(in) :: rho

  call dump_field( &
    filename, &
    mesh, &
    nspin, &
    rho, &
    rxyz, &
    iatype, &
    nzatom, &
    nelpsp, &
    ixyz0)
end subroutine bind_dump_field

