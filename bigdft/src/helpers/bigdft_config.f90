module module_bigdft_config
  use f_precisions, only: f_simple
  implicit none  

  private

  include 'configure.inc' !< Include variables set from configure.

  logical, parameter, public :: ASYNCconv = .true.

end module module_bigdft_config
