from sys import argv
from BigDFT import BioQM
from yaml import dump
logfile = argv[1]
bsys = BioQM.BioSystem.from_logfile(logfile, disable_warnings=True)
df = bsys.df
df['purities'] = [bsys.purities[f] for f in df['frag']]
print ('---\n'+dump(df.to_dict()))
